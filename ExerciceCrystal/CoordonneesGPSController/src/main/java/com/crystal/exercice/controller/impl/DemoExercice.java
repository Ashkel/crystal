package com.crystal.exercice.controller.impl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.crystal.exercice.*")
@EntityScan(basePackages = "com.crystal.exercice.entity")
public class DemoExercice {
    public static void main(String[] args) {
        SpringApplication.run(DemoExercice.class, args);
    }

}
