package com.crystal.exercice.controller.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crystal.exercice.controller.api.IControleurCoordonnees;
import com.crystal.exercice.controller.dto.BodyDistance;
import com.crystal.exercice.controller.dto.RetourAjouter;
import com.crystal.exercice.controller.dto.RetourDistance;
import com.crystal.exercice.controller.dto.RetourLister;
import com.crystal.exercice.controller.dto.RetourSupprimer;
import com.crystal.exercice.data.api.IDataCoordonnees;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSErrorCode;
import com.crystal.exercice.exception.CoordonneeGPSException;
@Controller
@RestController
@RequestMapping("/crystal")
public class ControleurCoordonnees implements IControleurCoordonnees {
    /**
     * Journalisation.
     */
    private Logger log = Logger.getLogger(getClass());
    /**
     * dependance vers la couche de persistance.
     */
    private IDataCoordonnees data;

    /**
     * Constructeur pour l'injection de la dependance par Spring.
     * 
     * @param paramData la dependance donnée par Spring.
     */
    @Autowired
    public ControleurCoordonnees(IDataCoordonnees paramData) {
        log.debug("Injection du Data dans le Controleur");
        data = paramData;
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping("/ajouter")
    public RetourAjouter ajouter(@RequestBody CoordonneeGPS paramCoord) {
        RetourAjouter retour = new RetourAjouter();
        if (isCoordNull(paramCoord)) {
            retour.setMessage("les coordonnées ne doivent pas etre null");
            retour.setCode(CoordonneeGPSErrorCode.COORDONNEE_NULL_INTERDIT.toString());
            log.debug(String.format("Coordonnées null : latitude: %s, longitude: %s", paramCoord.getLatitude(), paramCoord.getLongitude()));
        } else if (!isCoordBonFormat(paramCoord)) {
            retour.setMessage("les coordonnées doivent etre comprisent entre : -90 à 90 pour la latitude, et -180 à 180 pour la longitude");
            retour.setCode(CoordonneeGPSErrorCode.COORDONNEE_FORMAT.toString());
        }
        try {
            retour.setCoord(data.ajouter(paramCoord));
        } catch (CoordonneeGPSException e) {
            retour.setMessage(e.getMessage());
            retour.setCode(e.getCodeErreur().toString());
            log.debug(e);
        }
        return retour;
    }

    @Override
    @CrossOrigin(origins = "*")
    @GetMapping("/liste")
    public RetourLister lister() {
        RetourLister retour = new RetourLister();
        try {
            retour.setCoords(data.lister());
        } catch (CoordonneeGPSException e) {
            retour.setCode(e.getCodeErreur().toString());
            retour.setMessage(e.getMessage());
        }
        return retour;
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping("/supprimer")
    public RetourSupprimer supprimer(@RequestBody CoordonneeGPS paramCoord) {
        log.debug(paramCoord);
        log.debug(paramCoord.getLatitude());
        log.debug(paramCoord.getLongitude());
        RetourSupprimer retour = new RetourSupprimer();
        try {
            retour.setSupprime(data.supprimer(paramCoord));
            retour.setMessage("suppression ok");
        } catch (CoordonneeGPSException e) {
            log.debug(e);
            retour.setMessage("suppression pok");
            retour.setCode(e.getCodeErreur().toString());
            retour.setSupprime(false);
        }
        return retour;
    }

    @Override
    @CrossOrigin(origins = "*")
    @PostMapping("/distance")
    public RetourDistance calculDistance(@RequestBody BodyDistance paramCoords) {
        RetourDistance retour = new RetourDistance();
        if (isCoordNull(paramCoords.getCoordX()) || isCoordNull(paramCoords.getCoordY())) {
            retour.setMessage("les coordonnées ne doivent pas etre null");
        }
        Double pk = 180.0 / Math.PI;

        Double a1 = paramCoords.getCoordX().getLatitude() / pk;
        Double a2 = paramCoords.getCoordX().getLongitude() / pk;
        Double b1 = paramCoords.getCoordY().getLatitude() / pk;
        Double b2 = paramCoords.getCoordY().getLongitude() / pk;

        Double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        Double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        Double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        log.debug(IControleurCoordonnees.RAYON_TERRE * tt);
        
        retour.setDistance(Math.round((double)IControleurCoordonnees.RAYON_TERRE * tt) / 1000.0);
        
        return retour;
    }

    private boolean isCoordNull (CoordonneeGPS paramCoord) {
        if (paramCoord == null || paramCoord.getLatitude() == null || paramCoord.getLongitude() == null) {
            return true;
        }
        return false;
    }

    private boolean isCoordBonFormat(CoordonneeGPS paramCoord) {
        if(paramCoord.getLatitude() > IControleurCoordonnees.MAX_LATITUDE
                || paramCoord.getLatitude() < IControleurCoordonnees.MIN_LATITUDE
                || paramCoord.getLongitude() > IControleurCoordonnees.MAX_LONGITUDE
                || paramCoord.getLongitude() < IControleurCoordonnees.MIN_LONGITUDE) {
            return false;
        }
        return true;
    }
}
