/**
 * package contenant les tests de la couche controleur de l'application réalisée dans le cadre
 * de l'exercice. 
 */
package com.crystal.exercice.controler.test;
