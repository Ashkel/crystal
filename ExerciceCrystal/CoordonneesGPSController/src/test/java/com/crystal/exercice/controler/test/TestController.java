package com.crystal.exercice.controler.test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.crystal.exercice.controller.api.IControleurCoordonnees;
import com.crystal.exercice.controller.dto.BodyDistance;
import com.crystal.exercice.controller.dto.RetourAjouter;
import com.crystal.exercice.controller.dto.RetourLister;
import com.crystal.exercice.controller.dto.RetourSupprimer;
import com.crystal.exercice.controller.impl.ControleurCoordonnees;
import com.crystal.exercice.data.api.IDataCoordonnees;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSErrorCode;
import com.crystal.exercice.exception.CoordonneeGPSException;

@SpringBootTest(classes = ApplicationTestController.class)
public class TestController {
    private static Logger log = Logger.getLogger(TestController.class);
//    @Autowired
    private static ControleurCoordonnees controleur = new ControleurCoordonnees(null);
    private static CoordonneeGPS coordNominal = new CoordonneeGPS(45.85542, 122.55554);
    private static CoordonneeGPS coordIdentique1 = new CoordonneeGPS(41.40338, 2.17403);
    private static final CoordonneeGPSErrorCode ERREUR_DOUBLON = CoordonneeGPSErrorCode.COORDONNEE_EXISTE_DEJA;
    private static CoordonneeGPS coordExceptionNull = new CoordonneeGPS(null, 2.174035463);
    private static final CoordonneeGPSErrorCode ERREUR_NULL = CoordonneeGPSErrorCode.COORDONNEE_NULL_INTERDIT;
    private static final CoordonneeGPS coordExceptionTaille = new CoordonneeGPS(120.0, 120.0);
    private static final CoordonneeGPSErrorCode ERREUR_FORMAT = CoordonneeGPSErrorCode.COORDONNEE_FORMAT;
    private static final Integer TAILLE_LISTE = 6;
    private static final CoordonneeGPSErrorCode EXISTE_PAS = CoordonneeGPSErrorCode.COORDONNEE_N_EXISTE_PAS;
    private static CoordonneeGPS coordSuppressionNominale = new CoordonneeGPS(41.40338, 2.17403);
    private static CoordonneeGPS coordExistePas = new CoordonneeGPS(1.0, 2.0);
    private static CoordonneeGPS coordParisCentre = new CoordonneeGPS(48.856614, 2.3522219);
    private static CoordonneeGPS coordPorteChamperret = new CoordonneeGPS(48.8862829, 2.2903772);
    private static CoordonneeGPS coordLyon = new CoordonneeGPS(45.750000, 4.850000);
    private static BodyDistance coords = new BodyDistance(coordParisCentre, coordPorteChamperret);
    private static BodyDistance coords2 = new BodyDistance(coordParisCentre, coordLyon);
    private static IDataCoordonnees mockData = new IDataCoordonnees() {

        @Override
        public Boolean supprimer(CoordonneeGPS coord) throws CoordonneeGPSException {
            if (coord == coordSuppressionNominale) {
                return true;
            }
            throw new CoordonneeGPSException(EXISTE_PAS);
        }

        @Override
        public List<CoordonneeGPS> lister() throws CoordonneeGPSException {
            List<CoordonneeGPS> retour = new ArrayList<CoordonneeGPS>();
            for (int i = 0; i < TAILLE_LISTE; i++) {
                retour.add(new CoordonneeGPS());
            }
            return retour;
        }

        @Override
        public CoordonneeGPS ajouter(CoordonneeGPS coord) throws CoordonneeGPSException {
            if (coord == coordNominal) {
                return coordNominal;
            }
            if (coord == coordIdentique1) {
                throw new CoordonneeGPSException(ERREUR_DOUBLON);
            }
            return null;
        }
    };

    @BeforeAll
    public static void testLoads() {
        try {
            log.debug("avant test : ");
            Class<? extends IControleurCoordonnees> clazz = controleur.getClass();
            Field authField = clazz.getDeclaredField("data");
            authField.setAccessible(true);
            authField.set(controleur, mockData);
            authField.setAccessible(false);
        } catch (Exception e) {
            log.fatal(e);
        }
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testSupprimerNominal() {
        RetourSupprimer retour = controleur.supprimer(coordSuppressionNominale);
        Assert.assertNotNull(retour);
        Assert.assertNotNull(retour.getSupprime());
        Assert.assertTrue(retour.getSupprime());
        Assert.assertNotNull(retour.getMessage());

    }

    @Test
    public void testSupprimerException() {
        RetourSupprimer retour = controleur.supprimer(coordExistePas);
        Assert.assertNotNull(retour);
        Assert.assertFalse(retour.getSupprime());
        Assert.assertNotNull(retour.getMessage());
    }

    @Test
    public void testAjouterCoordonneeNominal() {
        RetourAjouter retour = controleur.ajouter(coordNominal);
        Assert.assertNotNull(coordNominal);
        Assert.assertEquals(coordNominal.getLatitude(), retour.getCoord().getLatitude());
        Assert.assertEquals(coordNominal.getLongitude(), retour.getCoord().getLongitude());
    }

    @Test
    public void testAjouterCoordonneeExceptionContrainteUnicite() {
        RetourAjouter retour = controleur.ajouter(coordIdentique1);
        Assert.assertEquals(ERREUR_DOUBLON.toString(), retour.getCode());
    }

    @Test
    public void testAjouterCoordonneeExceptionNull() {
        RetourAjouter retour = controleur.ajouter(coordExceptionNull);
        Assert.assertEquals(ERREUR_NULL.toString(), retour.getCode());
    }

    @Test
    public void testAjouterCoordonneeExceptionFormat() {
        RetourAjouter retour = controleur.ajouter(coordExceptionTaille);
        Assert.assertEquals(ERREUR_FORMAT.toString(), retour.getCode());
    }

    @Test
    public void testListe() {
        RetourLister retour = controleur.lister();
        Assert.assertEquals(TAILLE_LISTE.intValue(), retour.getCoords().size());
    }

    @Test
    public void testDistance() {
        log.debug(controleur.calculDistance(coords));
        log.debug(controleur.calculDistance(coords2));
    }
}
