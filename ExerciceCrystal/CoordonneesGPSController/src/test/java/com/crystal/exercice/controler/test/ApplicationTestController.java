package com.crystal.exercice.controler.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.crystal.exercice.*")
@EntityScan(basePackages = "com.crystal.exercice.entity")
public class ApplicationTestController {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationTestController.class, args);
    }
}
