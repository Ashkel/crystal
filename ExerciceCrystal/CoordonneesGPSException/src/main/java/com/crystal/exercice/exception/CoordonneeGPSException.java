package com.crystal.exercice.exception;

/**
 * Classe definissant les codes d'erreurs pour les exceptions de l'exercice.
 */
public class CoordonneeGPSException extends Exception {
    /**
     * version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le code d'erreur remonté par l'exception.
     */
    private CoordonneeGPSErrorCode codeErreur;

    /**
     * Constructeur par defaut.
     */
    public CoordonneeGPSException() {
    }

    /**
     * @param codeErreur le code d'erreur de l'exception.
     */
    public CoordonneeGPSException(CoordonneeGPSErrorCode paramCodeErreur) {
        super();
        codeErreur = paramCodeErreur;
    }

    /**
     * @param message le message de l'exception.
     */
    public CoordonneeGPSException(String paramMessage) {
        super(paramMessage);
    }

    /**
     * @param message le message de l'exception.
     * @param cause la trace de la cause de l'exception
     */
    public CoordonneeGPSException(String paramMessage, Throwable paramCause) {
        super(paramMessage, paramCause);
    }

    /**
     * @param message le message de l'exception.
     * @param codeErreur le code d'erreur de l'exception.
     */
    public CoordonneeGPSException(String paramMessage, CoordonneeGPSErrorCode paramCodeErreur) {
        super(paramMessage);
        codeErreur = paramCodeErreur;
    }
    /**
     * @return the codeErreur
     */
    public CoordonneeGPSErrorCode getCodeErreur() {
        return codeErreur;
    }

    /**
     * @param codeErreur the codeErreur to set
     */
    public void setCodeErreur(CoordonneeGPSErrorCode paramCodeErreur) {
        codeErreur = paramCodeErreur;
    }
}
