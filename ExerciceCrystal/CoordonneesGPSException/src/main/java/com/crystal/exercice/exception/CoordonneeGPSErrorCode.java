package com.crystal.exercice.exception;
/**
 * codes d'erreurs pour l'exercice.
 */
public enum CoordonneeGPSErrorCode {
    /**
     * cas ou l'on souhaite ajouter une coordonnée déjà présente dans l'unité de persistance.
     */
    COORDONNEE_EXISTE_DEJA,
    /**
     * cas ou l'on tente de supprimer une coordonnée qui n'existe pas dans l'unité de persistance.
     */
    COORDONNEE_N_EXISTE_PAS,
    /**
     * cas ou l'on tente d'ajouter une latitude ne respectant pas son format:<br />
     * <ul>
     *     <li>Le premier nombre indiqué en tant que latitude se situe entre -90 et 90.</li>
     *     <li>Le premier nombre indiqué en tant que longitude se situe entre -180 et 180.</li>
     * </ul>
     */
    COORDONNEE_FORMAT,
    /**
     * cas ou la latitude ou la longitude serait null.
     */
    COORDONNEE_NULL_INTERDIT,
    /**
     * cas ou l'unité de persistance ne repond pas.
     */
    CONNEXION_UNITE_DE_PERSISTANCE
}
