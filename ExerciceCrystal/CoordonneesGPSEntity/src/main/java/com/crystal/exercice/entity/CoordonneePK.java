package com.crystal.exercice.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Classe definissant la clé primaire de l'entité {@link CoordonneeGPS}.
 * On considère l'unicité d'un coordonnée par son couple de données latitude et longitude.
 */
public class CoordonneePK  implements Serializable {
    /**
     * Identifiant pour la sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Définition de la latitude de la coordonnée.
     */
    private Double latitude;
    /**
     * Définition de la longitude de la coordonnée.
     */
    private Double longitude;
    /**
     * Constructeur par defaut.
     */
    public CoordonneePK() {
    }
    /**
     * @param latitude la latitude de la clé.
     * @param longitude la longitude de la clé.
     */
    public CoordonneePK(Double paramLatitude, Double paramLongitude) {
        latitude = paramLatitude;
        longitude = paramLongitude;
    }
    
    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }
    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }
    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CoordonneePK other = (CoordonneePK) obj;
        return Objects.equals(latitude, other.latitude) && Objects.equals(longitude, other.longitude);
    }
}
