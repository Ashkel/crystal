/**
 * package contenant les entités manipulées par l'application réalisée dans le cadre
 * de l'exercice. 
 */
package com.crystal.exercice.entity;
