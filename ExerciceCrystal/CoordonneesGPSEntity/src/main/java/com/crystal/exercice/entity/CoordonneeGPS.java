package com.crystal.exercice.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

/**
 * Classe representant le Modèle de données pour des Coordonnées GPS.<br />
 * Les coordonnées seront manipulées sous forme de Degrés décimaux (DD) <br />
 * latitude et longitude<br />
 * <em> Le premier nombre indiqué en tant que latitude se situe entre -90 et
 * 90.<br />
 * Le premier nombre indiqué en tant que longitude se situe entre -180 et
 * 180.<br />
 * </em>
 */
@Entity
@IdClass(CoordonneePK.class)
@Table(name = "crystal_coordonnees")
public class CoordonneeGPS implements Serializable {
    /**
     * Identifiant pour la sérialisation. (Utilisé si on transporte nos entités via
     * RMI over HTTP / IIOP).
     */
    private static final long serialVersionUID = 1L;
    /**
     * Définition de la latitude de la coordonnée.
     */

    @Id
    @Column(name = "coordonnees_latitude", nullable = false)
    private Double latitude;
    /**
     * Définition de la longitude de la coordonnée.
     */
    @Id
    @Column(name = "coordonnees_longitude", nullable = false)
    private Double longitude;

    /**
     * Constructeur par default.
     */
    public CoordonneeGPS() {
    }

    /**
     * Constructeur surchargé permettant la création d'une coordonnée GPS avec sa
     * latitude et sa longitude.
     * 
     * @param latitude  la latitude de la coordonnée.
     * @param longitude la longitude de la coordonnée.
     */
    public CoordonneeGPS(Double paramLatitude, Double paramLongitude) {
        super();
        latitude = paramLatitude;
        longitude = paramLongitude;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param paramLatitude the latitude to set
     */
    public void setLatitude(Double paramLatitude) {
        latitude = paramLatitude;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param paramLongitude the longitude to set
     */
    public void setLongitude(Double paramLongitude) {
        longitude = paramLongitude;
    }
}
