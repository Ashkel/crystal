package com.crystal.exercice.data.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crystal.exercice.data.api.IDataCoordonnees;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.entity.CoordonneePK;
import com.crystal.exercice.exception.CoordonneeGPSErrorCode;
import com.crystal.exercice.exception.CoordonneeGPSException;

import jakarta.persistence.EntityManager;

/**
 * Implementation des services de persistance pour l'entité
 * {@link CoordonneeGPS}.
 */
@Repository
@Transactional(rollbackFor = CoordonneeGPSException.class)
public class DataCoordonnees implements IDataCoordonnees {    
    /**
     * Journalisation.
     */
    private Logger log = Logger.getLogger(getClass());
    /**
     * definition de l'entityManager permettant de gerer l'unité de persistance avec
     * Hibernate.
     */
    private EntityManager entityManager;

    /**
     * Constructeur pour l'injection de la dependance par Spring.
     * 
     * @param paramEntityManager la dependance donnée par Spring.
     */
    @Autowired
    public DataCoordonnees(EntityManager paramEntityManager) {
        log.debug("Injection de l'entityManager par Spring");
        entityManager = paramEntityManager;
    }

    @Override
    public CoordonneeGPS ajouter(CoordonneeGPS paramCoord) throws CoordonneeGPSException {
        try {
            entityManager.persist(paramCoord);
            entityManager.flush();
        } catch (ConstraintViolationException e) {
            if (e.getMessage().contains("null")) {
                throw new CoordonneeGPSException("Violation de contrainte",
                        CoordonneeGPSErrorCode.COORDONNEE_NULL_INTERDIT);
            } else {
                throw new CoordonneeGPSException("Violation de contrainte, la coordonnée existe déjà",
                        CoordonneeGPSErrorCode.COORDONNEE_EXISTE_DEJA);
            }
        }
        return paramCoord;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoordonneeGPS> lister() throws CoordonneeGPSException {
        return entityManager.createQuery("FROM CoordonneeGPS").getResultList();
    }

    @Override
    public Boolean supprimer(CoordonneeGPS paramCoord) throws CoordonneeGPSException {
        CoordonneeGPS attachCoord = entityManager.find(CoordonneeGPS.class,
                new CoordonneePK(paramCoord.getLatitude(), paramCoord.getLongitude()));
        if (attachCoord != null) {
            entityManager.remove(attachCoord);
        } else {
            throw new CoordonneeGPSException("Coordonnée n'existe pas",
                    CoordonneeGPSErrorCode.COORDONNEE_N_EXISTE_PAS);
        }
        return true;
    }

}
