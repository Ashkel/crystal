package com.crystal.exercice.test.data;

import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.crystal.exercice.data.api.IDataCoordonnees;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSErrorCode;
import com.crystal.exercice.exception.CoordonneeGPSException;

@SpringBootTest(classes = ApplicationTestLister.class)
public class TestDataListerSupprimerCoordonnees {
    private static final Integer NOMBRE_COORDONNEES = 7;
    private static final CoordonneeGPSErrorCode EXISTE_PAS =CoordonneeGPSErrorCode.COORDONNEE_N_EXISTE_PAS;
    private CoordonneeGPS coordNominal = new CoordonneeGPS(48.856614, 2.3522219);
    private CoordonneeGPS coordExistePas = new CoordonneeGPS(1.0, 2.0);
    @Autowired
    private IDataCoordonnees data;
    @Test
    void contextLoads() {
    }
    @Test
    public void testListerCoordonnees() {
        try {
            List<CoordonneeGPS> retour = data.lister();
            Assert.assertEquals(NOMBRE_COORDONNEES.intValue(), retour.size());
        } catch (CoordonneeGPSException e) {
            Assert.fail();
        }
    }
    @Test
    public void testSupprimerNominal() {
        try {
            data.supprimer(coordNominal);
        } catch (CoordonneeGPSException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void testSupprimerExistePas() {
        try {
            data.supprimer(coordExistePas);
            Assert.fail();
        } catch (CoordonneeGPSException e) {
            Assert.assertEquals(EXISTE_PAS, e.getCodeErreur());
        }
    }
}
