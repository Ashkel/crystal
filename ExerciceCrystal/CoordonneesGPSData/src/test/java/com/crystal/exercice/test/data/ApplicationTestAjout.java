package com.crystal.exercice.test.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.crystal.exercice.*")
@EntityScan(basePackages = "com.crystal.exercice.entity")
public class ApplicationTestAjout {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationTestAjout.class, args);
    }
}
