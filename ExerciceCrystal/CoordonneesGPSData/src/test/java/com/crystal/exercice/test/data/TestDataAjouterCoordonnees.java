package com.crystal.exercice.test.data;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.crystal.exercice.data.api.IDataCoordonnees;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSErrorCode;
import com.crystal.exercice.exception.CoordonneeGPSException;

@SpringBootTest(classes = ApplicationTestAjout.class)
public class TestDataAjouterCoordonnees {
    private CoordonneeGPS coordNominal = new CoordonneeGPS(45.85542, 122.55554);
    private CoordonneeGPS coordIdentique1 = new CoordonneeGPS(48.856614, 2.3522219);
    private static final CoordonneeGPSErrorCode ERREUR_DOUBLON = CoordonneeGPSErrorCode.COORDONNEE_EXISTE_DEJA;
    private CoordonneeGPS coordExceptionNull = new CoordonneeGPS(null, 2.174035463);
    private static final CoordonneeGPSErrorCode ERREUR_NULL = CoordonneeGPSErrorCode.COORDONNEE_NULL_INTERDIT;
    
    @Autowired
    private IDataCoordonnees data;
    @Test
    void contextLoads() {
    }
    @Test
    public void testAjouterCoordonneeNominal() {
        try {
            CoordonneeGPS retour = data.ajouter(coordNominal);
            Assert.assertNotNull(coordNominal);
            Assert.assertEquals(coordNominal.getLatitude(), retour.getLatitude());
            Assert.assertEquals(coordNominal.getLongitude(), retour.getLongitude());
        } catch (CoordonneeGPSException e) {
            Assert.fail();
        }
    }
    @Test
    public void testAjouterCoordonneeExceptionContrainteUnicite() {
        try {
            data.ajouter(coordIdentique1);
            Assert.fail();
        } catch (CoordonneeGPSException e) {
            Assert.assertEquals(ERREUR_DOUBLON, e.getCodeErreur());
        }
    }
    @Test
    public void testAjouterCoordonneeExceptionNull() {
        try {
            data.ajouter(coordExceptionNull);
            Assert.fail();
        } catch (CoordonneeGPSException e) {
            Assert.assertEquals(ERREUR_NULL, e.getCodeErreur());
        }
    }
}
