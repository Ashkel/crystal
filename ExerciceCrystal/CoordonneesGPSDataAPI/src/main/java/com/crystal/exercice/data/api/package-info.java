/**
 * package contenant les définitions des méthodes de la couche controleur REST de l'application réalisée dans le cadre
 * de l'exercice. 
 */
package com.crystal.exercice.data.api;
