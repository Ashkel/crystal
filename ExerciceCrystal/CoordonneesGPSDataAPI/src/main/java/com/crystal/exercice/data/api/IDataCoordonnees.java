package com.crystal.exercice.data.api;

import java.util.List;

import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSException;
/**
 * Définition des services de persistance de l'entité {@link CoordonneeGPS}.
 */
public interface IDataCoordonnees {
    /**
     * Méthode pour ajouter une {@link CoordonneeGPS} dans l'unité de persistance.
     * @param coord la coordonnée à ajouter.
     * @return la coordonnée ajoutée.
     * @throws CoordonneeGPSException lors d'une erreur liée à la persistance.
     * <ul>
     *  <li>Latitude au mauvais format pour l'unité de persistance : null</li>
     *  <li>Longitude au mauvais format pour l'unité de persistance : null</li>
     *  <li>La coordonnée avec son couple latitude, longitude existe déjà</li>
     *  <li>l'unité de persistance ne repond pas</li>
     * </ul>
     */
    CoordonneeGPS ajouter(CoordonneeGPS coord) throws CoordonneeGPSException;
    /**
     * Méthode pour lister les {@link CoordonneeGPS} de l'unité de persistance.
     * @return une {@link List} de {@link CoordonneeGPS} contenues dans l'unité de persistance.<br />
     * Une {@link List} vide si l'unité de persistance ne possède pas de coordonnées GPS.
     * @throws CoordonneeGPSException
     * <ul>
     *  <li>l'unité de persistance ne repond pas</li>
     * </ul>
     */
    List<CoordonneeGPS> lister() throws CoordonneeGPSException;
    /**
     * Méthode pour supprimer une {@link CoordonneeGPS} de l'unité de persistance.
     * @param coord la {@link CoordonneeGPS} à supprimer.
     * @return
     * <ul>
     *  <li>true si la suppression est bien réalisée</li>
     * </ul>
     * @throws CoordonneeGPSException
     * <ul>
     *  <li>la coordonnée n'existe pas dans l'unité de persistance</li>
     *  <li>l'unité de persistance ne repond pas</li>
     * </ul>
     */
    Boolean supprimer(CoordonneeGPS coord) throws CoordonneeGPSException;
}
