/**
 * package contenant les méthodes de la couche de persistance de l'application réalisée dans le cadre
 * de l'exercice. 
 */
package com.crystal.exercice.controller.api;
