package com.crystal.exercice.controller.api;

import java.util.List;

import com.crystal.exercice.controller.dto.BodyDistance;
import com.crystal.exercice.controller.dto.RetourAjouter;
import com.crystal.exercice.controller.dto.RetourDistance;
import com.crystal.exercice.controller.dto.RetourLister;
import com.crystal.exercice.controller.dto.RetourSupprimer;
import com.crystal.exercice.entity.CoordonneeGPS;
import com.crystal.exercice.exception.CoordonneeGPSException;

/**
 * Définition des services REST pour la manipulation de l'entité {@link CoordonneeGPS}.
 */
public interface IControleurCoordonnees {
    /**
     * Rayon de la terre pour le calcul de distance.
     */
    static final Double RAYON_TERRE = 6378137D;
    /**
     * Valeur minimale pour la latitude.
     */
    static final Integer MIN_LATITUDE = -90;
    /**
     * Valeur maximale pour la latitude.
     */
    static final Integer MAX_LATITUDE = 90;
    /**
     * Valeur minimale pour la longitude.
     */
    static final Integer MIN_LONGITUDE = -180;
    /**
     * Valeur maximale pour la longitude.
     */
    static final Integer MAX_LONGITUDE = 180;
    /**
     * Méthode pour ajouter une {@link CoordonneeGPS} dans l'application.
     * @param coord la coordonnée à ajouter.
     * @return la coordonnée ajoutée.
     * @throws CoordonneeGPSException lors d'une erreur liée à l'ajout.
     * <ul>
     *  <li>Latitude au mauvais format : entre -90 et 90</li>
     *  <li>Longitude au mauvais format : entre -180 et 180</li>
     *  <li>erreur remontée de la couche de persistance</li>
     * </ul>
     */
    RetourAjouter ajouter(CoordonneeGPS coord) throws CoordonneeGPSException;
    /**
     * Méthode pour lister les {@link CoordonneeGPS} de l'application.
     * @return une {@link List} de {@link CoordonneeGPS} contenues dans l'application.<br />
     * Une {@link List} vide si l'application ne possède pas de coordonnées GPS.
     * @throws CoordonneeGPSException
     * <ul>
     *  <li>erreur remontée de la couche de persistance</li>
     * </ul>
     */
    RetourLister lister() throws CoordonneeGPSException;
    /**
     * Méthode pour supprimer une {@link CoordonneeGPS} de l'application.
     * @param coord la {@link CoordonneeGPS} à supprimer.
     * @return
     * <ul>
     *  <li>true si la suppression est bien réalisée</li>
     * </ul>
     * @throws CoordonneeGPSException
     * <ul>
     *  <li>erreur remontée de la couche de persistance</li>
     * </ul>
     */
    RetourSupprimer supprimer(CoordonneeGPS coord);

    /**
     * Méthode pour calculer la distance entre deux coordonnées.
     * @param coordX la premiere coordonnée
     * @param coordY la seconde coordonnée
     * @return la distance en km
     * @throws CoordonneeGPSException
     * <ul>
     *  <li>si l'une des coordonnées possède des informations erronées; latitude et / ou longitude</li>
     * </ul>
     */
    RetourDistance calculDistance(BodyDistance coords);
}
