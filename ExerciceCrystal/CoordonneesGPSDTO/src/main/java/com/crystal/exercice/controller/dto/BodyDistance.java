package com.crystal.exercice.controller.dto;

import java.io.Serializable;

import com.crystal.exercice.entity.CoordonneeGPS;

/**
 * Définition du message JSON attendu pour la methode distance.
 */
public class BodyDistance implements Serializable {
    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * la permiere coordonnée.
     */
    private CoordonneeGPS coordX;
    /**
     * la seconde coordonnée.
     */
    private CoordonneeGPS coordY;
    
    /**
     * Constructeur par defaut.
     */
    public BodyDistance() {
    }

    /**
     * @param paramCoordX
     * @param paramCoordY
     */
    public BodyDistance(CoordonneeGPS paramCoordX, CoordonneeGPS paramCoordY) {
        super();
        coordX = paramCoordX;
        coordY = paramCoordY;
    }

    /**
     * @return the coordX
     */
    public CoordonneeGPS getCoordX() {
        return coordX;
    }

    /**
     * @param paramCoordX the coordX to set
     */
    public void setCoordX(CoordonneeGPS paramCoordX) {
        coordX = paramCoordX;
    }

    /**
     * @return the coordY
     */
    public CoordonneeGPS getCoordY() {
        return coordY;
    }

    /**
     * @param paramCoordY the coordY to set
     */
    public void setCoordY(CoordonneeGPS paramCoordY) {
        coordY = paramCoordY;
    }

}
