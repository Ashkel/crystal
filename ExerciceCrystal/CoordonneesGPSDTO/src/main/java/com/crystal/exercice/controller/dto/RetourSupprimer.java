package com.crystal.exercice.controller.dto;

/**
 * Définition du retour JSON de la methode supprimer.
 */
public class RetourSupprimer extends RetourGenerique {

    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Defini si la suppression a été réalisée.
     */
    private Boolean supprime;

    /**
     * Constructeur par defaut. 
     */
    public RetourSupprimer() {
        super();
    }

    /**
     * @param paramSupprime
     * @param paramMessage
     */
    public RetourSupprimer(Boolean paramSupprime, String paramMessage) {
        super(paramMessage);
        supprime = paramSupprime;
    }

    /**
     * @return the supprime
     */
    public Boolean getSupprime() {
        return supprime;
    }

    /**
     * @param paramSupprime the supprime to set
     */
    public void setSupprime(Boolean paramSupprime) {
        supprime = paramSupprime;
    }

}
