/**
 * package contenant les définition des objets manipulés par les services REST de l'application réalisée dans le cadre
 * de l'exercice. 
 */
package com.crystal.exercice.controller.dto;
