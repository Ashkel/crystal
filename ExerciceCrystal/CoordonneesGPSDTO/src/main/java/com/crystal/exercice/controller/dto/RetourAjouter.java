package com.crystal.exercice.controller.dto;

import com.crystal.exercice.entity.CoordonneeGPS;
/**
 * Définition du message JSON retourné pour la methode ajouter.
 */
public class RetourAjouter extends RetourGenerique {
    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * La coordonnée ajoutés.
     */
    private CoordonneeGPS coord;

    /**
     * Constructeur par defaut.
     */
    public RetourAjouter() {
        super();
    }

    /**
     * @param paramCoord
     * @param message
     */
    public RetourAjouter(CoordonneeGPS paramCoord, String paramMessage) {
        super(paramMessage);
        coord = paramCoord;
    }

    /**
     * @return the coord
     */
    public CoordonneeGPS getCoord() {
        return coord;
    }

    /**
     * @param coord the coord to set
     */
    public void setCoord(CoordonneeGPS paramCoord) {
        coord = paramCoord;
    }

}
