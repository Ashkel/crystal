package com.crystal.exercice.controller.dto;

import java.util.List;

import com.crystal.exercice.entity.CoordonneeGPS;
/**
 * Définition du message JSON retourné pour la methode ajouter.
 */
public class RetourLister extends RetourGenerique {
    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * La coordonnée ajoutés.
     */
    private List<CoordonneeGPS> coords;

    /**
     * Constructeur par defaut.
     */
    public RetourLister() {
        super();
    }

    /**
     * @param paramCoord
     * @param message
     */
    public RetourLister(List<CoordonneeGPS> paramCoords, String paramMessage) {
        super(paramMessage);
        coords = paramCoords;
    }

    /**
     * @return the coord
     */
    public List<CoordonneeGPS> getCoords() {
        return coords;
    }

    /**
     * @param coord the coord to set
     */
    public void setCoords(List<CoordonneeGPS> paramCoords) {
        coords = paramCoords;
    }

}
