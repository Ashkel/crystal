package com.crystal.exercice.controller.dto;

import java.io.Serializable;

/**
 * Définition du retour des services rest.
 * Il possèderont un message pour fournir des informations venant de l'application.
 */
public class RetourGenerique implements Serializable {

    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le message de l'application.
     */
    private String message;
    /**
     * Code d'erreur de l'application.
     */
    private String code;
    /**
     * 
     */
    public RetourGenerique() {
    }
    /**
     * @param paramMessage
     */
    public RetourGenerique(String paramMessage) {
        super();
        message = paramMessage;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param paramMessage the message to set
     */
    public void setMessage(String paramMessage) {
        message = paramMessage;
    }
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    /**
     * @param code the code to set
     */
    public void setCode(String paramCode) {
        code = paramCode;
    }

}
