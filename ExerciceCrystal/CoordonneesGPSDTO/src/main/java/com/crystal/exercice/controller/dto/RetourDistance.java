package com.crystal.exercice.controller.dto;

/**
 * Définition du retour JSON de la methode distance.
 */
public class RetourDistance extends RetourGenerique {

    /**
     * Version pour la serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * La distance calculée entre les deux coordonnées.
     */
    private Double distance;

    /**
     * Constructeur par defaut.
     */
    public RetourDistance() {
        super();
    }

    /**
     * @param distance
     * @param message
     */
    public RetourDistance(Double paramDistance, String paramMessage) {
        super(paramMessage);
        distance = paramDistance;
    }

    /**
     * @return the distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(Double paramDistance) {
        distance = paramDistance;
    }

}
