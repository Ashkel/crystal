import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AjouterCoordonneeComponent } from './composants/ajouter-coordonnee/ajouter-coordonnee.component';
import { ListerCoordonneesComponent } from './composants/lister-coordonnees/lister-coordonnees.component';
import { DistanceCoordonneesComponent } from './composants/distance-coordonnees/distance-coordonnees.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AjouterCoordonneeComponent,
    ListerCoordonneesComponent,
    DistanceCoordonneesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
