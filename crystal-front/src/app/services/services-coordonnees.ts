import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Coordonnee } from '../dto/coordonnee';
import { Observable } from 'rxjs';
import { RetourAjouter } from '../dto/retour-ajouter';
import { RetourDistance } from '../dto/retour-distance';
import { RetourSupprimer } from '../dto/retour-supprimer';
import { BodyDistance } from '../dto/body-distance';
import { RetourListe } from '../dto/retour-liste';

@Injectable({
    providedIn: 'root'
})
export class CoordonneesServices {
    urlAjouter: string = environment.restController + environment.ajouterCoordonnee;
    urlSupprimer: string = environment.restController + environment.supprimerCoordonnee;
    urlLister: string = environment.restController + environment.listerCoordonnees;
    urlDistance: string = environment.restController + environment.distanceCoordonnees;
    
    constructor(private http: HttpClient) { }

    ajouterCoordonnee(coord: Coordonnee): Observable<RetourAjouter> {
        return this.http.post<RetourAjouter>(this.urlAjouter, coord);
    }

    lister(): Observable<RetourListe> {
        return this.http.get<RetourAjouter>(this.urlLister);
    }

    supprimerCoordonnee(coord: Coordonnee): Observable<RetourSupprimer> {
        return this.http.post<RetourSupprimer>(this.urlSupprimer, coord);
    }

    distanceCoordonnees(coords: BodyDistance): Observable<RetourDistance> {
        return this.http.post<RetourDistance>(this.urlDistance, coords);
    }
}