import { Component, OnInit } from '@angular/core';
import { CoordonneesServices } from 'src/app/services/services-coordonnees';
import { Coordonnee } from 'src/app/dto/coordonnee';
import { RetourDistance } from 'src/app/dto/retour-distance';
import { BodyDistance } from 'src/app/dto/body-distance';

@Component({
  selector: 'app-distance-coordonnees',
  templateUrl: './distance-coordonnees.component.html',
  styleUrls: ['./distance-coordonnees.component.css']
})
export class DistanceCoordonneesComponent implements OnInit {
  coordX?: Coordonnee = {latitude: null, longitude: null};
  coordY?: Coordonnee = {latitude: null, longitude: null};
  retourCalcul?: RetourDistance;
  bodyCalul?: BodyDistance = {coordX: null, coordY: null};
  coordsX: Coordonnee[];
  coordsY: Coordonnee[] = [];
  coordTmpX: Number[];
  coordTmpY: Number[];
  message: String;
  constructor(private serviceCoordonnees: CoordonneesServices) { }

  ngOnInit() {
    this.serviceCoordonnees.lister().subscribe(
      data => {
        this.coordsX = data.coords;
        this.coordsY = [];
        this.coordsX.forEach(val => this.coordsY.push(Object.assign({}, val)));
        this.coordX.latitude = this.coordsX[0].latitude;
        this.coordX.longitude = this.coordsX[0].longitude;
        this.coordY.latitude = this.coordsY[0].latitude;
        this.coordY.longitude = this.coordsY[0].longitude;
        console.log(this.coordsX === this.coordsY)
        this.calculDistance();
      }
    );
  }
  calculDistance() {
    this.bodyCalul.coordX = this.coordX;
    this.bodyCalul.coordY = this.coordY;
    this.serviceCoordonnees.distanceCoordonnees(this.bodyCalul).subscribe(
      data => {
        console.log(data)
        if(data.distance > 10) {
          this.message = "plus de 10Km (" + data.distance + ")";
        } else {
          this.message = "moins de 10Km (" + data.distance + ")";
        }
      }
    );
  }
  getSelectedValueX(event:any) {
    this.coordTmpX = event.target.value.split('-');
    this.coordX.latitude = Number(this.coordTmpX[0]);
    this.coordX.longitude = Number(this.coordTmpX[1]);
    console.log(this.coordX)
    console.log(this.coordY)
    this.calculDistance();
  }
  getSelectedValueY(event:any) {
    this.coordTmpY = event.target.value.split('-');
    this.coordY.latitude = Number(this.coordTmpY[0]);
    this.coordY.longitude = Number(this.coordTmpY[1]);
    
    console.log(this.coordX)
    console.log(this.coordY)
    this.calculDistance();
  }
}
