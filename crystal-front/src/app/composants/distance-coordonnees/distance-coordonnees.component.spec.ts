import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistanceCoordonneesComponent } from './distance-coordonnees.component';

describe('DistanceCoordonneesComponent', () => {
  let component: DistanceCoordonneesComponent;
  let fixture: ComponentFixture<DistanceCoordonneesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistanceCoordonneesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistanceCoordonneesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
