import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterCoordonneeComponent } from './ajouter-coordonnee.component';

describe('AjouterCoordonneeComponent', () => {
  let component: AjouterCoordonneeComponent;
  let fixture: ComponentFixture<AjouterCoordonneeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterCoordonneeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterCoordonneeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
