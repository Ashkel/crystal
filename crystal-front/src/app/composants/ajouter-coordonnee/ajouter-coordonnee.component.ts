import { Component, OnInit, Input } from '@angular/core';
import { CoordonneesServices } from 'src/app/services/services-coordonnees';
import { Coordonnee } from 'src/app/dto/coordonnee';
import { RetourAjouter } from 'src/app/dto/retour-ajouter';
import { isNumber } from 'util';
import { ListerCoordonneesComponent } from '../lister-coordonnees/lister-coordonnees.component';
import { DistanceCoordonneesComponent } from '../distance-coordonnees/distance-coordonnees.component';


@Component({
  selector: 'app-ajouter-coordonnee',
  templateUrl: './ajouter-coordonnee.component.html',
  styleUrls: ['./ajouter-coordonnee.component.css']
})
export class AjouterCoordonneeComponent implements OnInit {
  message: string;
  coord?: Coordonnee = {latitude: null, longitude: null};
  retour?: RetourAjouter;
  @Input() listerComposant: ListerCoordonneesComponent;
  @Input() distanceComposant: DistanceCoordonneesComponent;
  constructor(private serviceCoordonnees: CoordonneesServices) { }

  ngOnInit() {
  }

  ajouter() {
    console.log(isNaN(this.coord.latitude));
    console.log(isNaN(this.coord.longitude));
    if(!isNaN(this.coord.latitude) && !isNaN(this.coord.longitude)) {
      this.serviceCoordonnees.ajouterCoordonnee(this.coord).subscribe(
        data => {
          this.retour = data;
          console.log(this.retour); 
          if(this.retour.code != null) {
            this.message = this.retour.message.toString();
          } else {
            this.message = "coordonnées bien ajoutées";
            this.listerComposant.lister();
            this.distanceComposant.ngOnInit();
          }
        }
      );
    } else {
      this.message = "Saisir des nombres pour les coordonnées.";
    }
  }

}
