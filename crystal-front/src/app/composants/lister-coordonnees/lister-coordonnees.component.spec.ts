import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListerCoordonneesComponent } from './lister-coordonnees.component';

describe('ListerCoordonneesComponent', () => {
  let component: ListerCoordonneesComponent;
  let fixture: ComponentFixture<ListerCoordonneesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListerCoordonneesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListerCoordonneesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
