import { Component, OnInit, Input } from '@angular/core';
import { CoordonneesServices } from 'src/app/services/services-coordonnees';
import { Coordonnee } from 'src/app/dto/coordonnee';
import { RetourSupprimer } from 'src/app/dto/retour-supprimer';

@Component({
  selector: 'app-lister-coordonnees',
  templateUrl: './lister-coordonnees.component.html',
  styleUrls: ['./lister-coordonnees.component.css']
})
export class ListerCoordonneesComponent implements OnInit {
  coords: Coordonnee[] = [];
  retour?: RetourSupprimer;
  @Input() selectedCoord: Coordonnee;
  message: string;
  constructor(private serviceCoordonnees: CoordonneesServices) { }
  ngOnInit() {
  }

  lister() {
    this.serviceCoordonnees.lister().subscribe(
      data => {
        this.coords = data.coords;
      }
    );
  }
  supprimer(coord:Coordonnee) {
    this.selectedCoord = coord;
    this.serviceCoordonnees.supprimerCoordonnee(this.selectedCoord).subscribe(
      data => {
        if(data.code != null) {
          this.message = data.message.toString();
        } else {
          this.lister();
        }
      }
    );
  }
}
