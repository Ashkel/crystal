import { Coordonnee } from './coordonnee';

export interface RetourDistance {
    message?: String,
    code?: String,
    distance?: number
}