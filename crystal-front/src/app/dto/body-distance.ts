import { Coordonnee } from './coordonnee';

export interface BodyDistance {
    coordX: Coordonnee,
    coordY: Coordonnee   
}