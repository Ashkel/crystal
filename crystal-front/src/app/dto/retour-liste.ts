import { Coordonnee } from './coordonnee';

export interface RetourListe {
    message?: String,
    code?: String,
    coords?: Coordonnee[]
}