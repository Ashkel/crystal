import { Coordonnee } from './coordonnee';

export interface RetourAjouter {
    message?: String,
    code?: String,
    coord?: Coordonnee
}