export interface Coordonnee {
    latitude: number,
    longitude: number
}