export interface RetourSupprimer {
    message?: string,
    code?: string,
    supprime?: boolean
}